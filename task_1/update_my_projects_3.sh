#!/bin/bash
projects_dir="projects_dir"

init () {
    if [ -d $projects_dir ];
    then
        echo "Yes, if there is a directory ${projects_dir}"
    
    else
        echo "No, there is no a directory ${projects_dir}"
        mkdir $projects_dir
    fi
    cd $projects_dir
}

cloning(){
    list_of_repository=$(cat ../list_of_repository.txt)
    for repository in $list_of_repository; 
    do 
        git clone $repository
        if [ $? = 0 ];
        then
            echo "clone success of ${repository%/}"
        else
            echo "Error, clone failure of ${repository%/}"
            exit 1
        fi
    done
}

upgrade(){
    list=$(ls -d */)
    for project in $list
    do
        cd $project
        git fetch origin master
        git reset
        git pull origin master
        if [ $? = 0 ];
        then
            echo "Upgrade success of ${project%/}"
        else
            echo "Error, upgrade failure of ${project%/}"
            exit 1
        fi
        cd ..
    done
}

compiling(){
    list=$(ls -d */)
    for project in $list
    do
        cd $project
        mvn install -DskipTests
        if [ $? = 0 ];
        then
            echo "Build success of ${project%/}"
        else
            echo "Error, build failure of ${project%/}"
            exit 1
        fi
        cd ..
    done
}

init

case $1 in
    1)
        echo "cloning"
        cloning
    ;;
    2)
        echo "upgrade"
        upgrade
    ;;
    3)
        echo "compiling"
        compiling
    ;;
    4)
        echo "cloning and compiling"   
        cloning
        compiling
    ;;
    5)
        echo "upgrade and compiling"   
        upgrade
        compiling
    ;;
    *)
        if [ $# = 0 ];
        then
            echo "cloning, upgrade and compiling"
            cloning
            upgrade
            compiling
        else
            echo "Option not valid"
            exit 1
        fi
    ;;
esac

exit 0