#!/bin/bash
list=$(ls -d */)
for var in $list
do
    cd $var
    mvn clean install
    if [ $? = 0 ];
    then
        echo "Build success of ${var%/}"
    else
        echo "Error, build failure of ${var%/}"
        exit 1
    fi
    cd ..
done
exit 0