#!/bin/bash
projects_dir="projects_dir"

init () {
    if [ -d $projects_dir ];
    then
        echo "Yes, if there is a directory ${projects_dir}"
    
    else
        echo "No, there is no a directory ${projects_dir}"
        mkdir $projects_dir
    fi
    cd $projects_dir
}

cloning(){
    list_of_repository=$(cat ../list_of_repository.txt)
    for repository in $list_of_repository; 
    do 
        git clone $repository
        if [ $? = 0 ];
        then
            echo "clone success of ${repository%/}"
        else
            echo "Error, clone failure of ${repository%/}"
            exit 1
        fi
    done
}

compiling(){
    list=$(ls -d */)
    for var in $list
    do
        cd $var
        mvn install -DskipTests
        if [ $? = 0 ];
        then
            echo "Build success of ${var%/}"
        else
            echo "Error, build failure of ${var%/}"
            exit 1
        fi
        cd ..
    done
}

init

case $1 in
    0)
        echo "cloning"
        cloning
    ;;
    1)
        echo "compiling"
        compiling
    ;;
    2)
        echo "cloning and compiling"   
        cloning
        compiling
    ;;
    *)
        echo "Option not valid"
    ;;
esac

exit 0